// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getStorage } from "firebase/storage";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyD9XZDDxaPGB6aO4H19wXlQlDRE0_wg_QA",
  authDomain: "rainbowdrive-3419d.firebaseapp.com",
  projectId: "rainbowdrive-3419d",
  storageBucket: "rainbowdrive-3419d.appspot.com",
  messagingSenderId: "590017578055",
  appId: "1:590017578055:web:7e1feb0b23485454f95d62",
  measurementId: "G-N70ZLZYDTC"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
export const database = getFirestore(app);
